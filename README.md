# Notify on This Folder Not That Folder (NTFNTF)

![NTFNTF icon](src/assets/images/icon-256x256.png)

**NTFNTF** is a Thunderbird extension allowing per-account and per-folder configuration of notifications and alerts, giving you the flexibility to choose which mailbox notifications you want to be alerted about and how they alert you.

I use multiple email addresses and found that I don't necessarily want to be alerted on all incoming mail. I created this to help me. If its useful to you too consider a coffee:  
[![Buy Me a Coffee](src/assets/images/bmc-button.png)](https://www.buymeacoffee.com/dansul)

![NTFNTF Settings 1](src/assets/images/TBNTFSettings1.png)

![NTFNTF Settings 2](src/assets/images/TBNTFSettings2.png)

## Features

- Supports multiple accounts.
- Change alert sounds and volumes per account.
- Apply a custom sound to each account.
- Different levels of details in notifications per account, including no text notifications but keeping sound alerts.
- Disable notifications entirely for individual accounts.
- Runs after filters have applied, so moved mail's notifications can be stifled.
- Uses native notifications.
- Supports Mac, Windows and Linux - see set up instructions for your system.

## Installation

You can install the **NTFNTF** extension from either the Thunderbird Add-ons store, downloads from Gitlab or by building it yourself. 
Choose an install method and then continue following the other setup instructions for your Operating System.

### From Thunderbird's Addons Store

- Go to Thunderbird Settings
- Go to Add-ons and Themes
- Choose Extensions
- Search for NTFNTF in "Find more add-ons"

### From Gitlab Releases

- Go to this project's [releases page](https://gitlab.com/dan-sullivan/ntfntf/-/releases)
- Find the latest or release (or an earlier one if you need)
- Find the `ntfntf-<version>.zip` under "Other" and click to download
- Proceed to the step: "Load the Extension into Thunderbird"

### From Building it Yourself

- Follow these build steps

```sh
# Clone the repository to your local machine using:
git clone https://gitlab.com/dan-sullivan/ntfntf

# Navigate into the cloned directory:
cd ntfntf

# Install the dependencies:
npm install

# Build the project:
npm run build
```

- This will have created an `ntfntf.zip` file in the `web-ext-artifacts` directory.
- Proceed to the step: "Load the Extension into Thunderbird"

### Load the Extension into Thunderbird 

Follow these steps only if you didn't install from the Add-ons store.

- Open Thunderbird's Settings
- Choose Add-ons and Themes
- Go to Extensions
- Click the Cog/Gear icon and select "Install Add-on From File"
- Navigate to and select the NTFNTF created or downloaded earlier
- Now you need to Configure Your System

### Configure Your System

As NTFNTF takes over notification and alerting responsibilities for mail, you need to disable the existing functionality for Thunderbird. This differs by Operating System.

#### Windows and Linux

- Open Thunderbird Settings
- Under General, scroll down to the "Incoming Mail" section
- If on *Windows*, enable "Use the system notification"
- Disable the following:
  - Show and Alert
  - Play a Sound
- You're now all set to configure which folders and accounts to alert for in the NTFNTF Extension settings. See "Usage".

**Linux**
![Windows TB Setting to Disable](src/assets/images/LinDisable.png)

**Windows**
![Windows TB Setting to Disable](src/assets/images/WinDisable.png)

#### MacOS 

Mac is a little more involved to disable existing alerting as Thunderbird delegates the settings to MacOS. So we must disable there and also disable a setting hidden on the MacOS Thunderbird builds.

**Disable System Sound Alerts for Thunderbird**

- Open the MacOS System Settings
- Search for Notifications
- Choose "Notification settings"

![MacOS Notifcation Settings](src/assets/images/MacSettingsNotif.png)

- Scroll down and select Thunderbird from the list of apps
- Disable "Play sound for notification"

![MacOS Disable Sound Alert](src/assets/images/MacTBSound.png)

- Close out of System Settings

**Disable Thunderbird Built-in Mail Notifications**

- Open Thunderbird Settings
- Scroll all the way to the bottom of General
- Click the Config Editor button
- Search for `mail.biff.show_alert`
- Toggle this so it is set to False
  
![MacOS Disable Mail Notification](src/assets/images/MacAdvSettings.png)

- You're now all set to configure which folders and accounts to alert for in the NTFNTF Extension settings. See "Usage".

## Usage

After installing the NTFNTF extension, configure it by navigating to the extension options within Thunderbird’s Add-on Manager. Here, you can select which folders you want to receive notifications for.  

### Global Settings

In this top section you can choose to disable the entire extension - this will keep your account and folder settings.
You can also choose to Reset All which will set all folders and accounts back to defaults.  

### Account Settings

The various options should be fairly obvious. Choose your sound and notification details for each account.  
Use the `All / None` links to select or de-select all sub-folders.


## Known Limitations

### Local Folders are not supported

Local Folders and RSS feeds are unable to trigger notifications and so are filtered from the account list. This is unfortunately a limitation with Thunderbird's API and Im yet to think of a workaround.

### Some Folders don't generate notifications

If a folder doesnt generate notifications it may be that messages are moved there before syncing to Thunderbird. You can try the following.  

- Right click the affected folder
- Select `Properties`
- Check the box for `When getting messages for this account always check this folder` and save

## Contributing

Contributions to NTFNTF are welcome! Whether it's bug reports, feature requests, or code contributions, please feel free to reach out or submit a pull request.

### To contribute code:

1. Fork the repository.
2. Install dependencies (`npm install`)
   1. NOTE: This will install git hooks for the project. You can see what is run in the `scripts.prepare` key in `package.json`
3. Create a new branch for your feature (`git checkout -b feature/MyFeature`).
4. Commit your changes (`git commit -m 'Add some Feature'`).
5. Push to the branch (`git push origin feature/MyFeature`).
6. Open a Merge Request.

Note: The project uses [Standard JS](https://standardjs.com/) by way of an enforced style.

### Helper scripts

Run these with `npm run <script>`

| Command       | Description                                                                                   |
|---------------|-----------------------------------------------------------------------------------------------|
| `js-build`    | Build the JS portion - outputs to `dist/`                                                     |
| `ext-build`   | Build the extension zip file from `dist/` - outputs to `web-ext-artifacts`                   |
| `standard`    | Run Standard JS linter against the js files                                                   |
| `fix`         | Try to auto-fix any Standard JS linting issues in the js files                                |
| `build`       | Build the JS portion and the Web Extension (`js-build` + `ext-build`)                         |
| `mock-build`  | Like `build` but uses mock data so HTML can be used a normal browser, useful for styling the options page |
| `test`        | Run the test suite                                                                            |
| `serve`       | Serve the `dist/` folder on http://127.0.0.1:1234                                             |

### Development Tips

- If working on the options pages, use `npm run serve` which will add mock data and rebuild the HTML pages, allowing you to use a normal browser to help development
- When working inside Thunderbird, you can add the `dist/` directory as a temporary extension for development by choosing "Debug Add-ons" from the Manage your Extensions gear menu and then "Load Temporary Add-on". Select `dist/manifest.json` and you can now use the reload button when you've made changes instead of re-installing the from built zip.

## License

This project is under the MIT License. See the LICENSE file for the full license text.
