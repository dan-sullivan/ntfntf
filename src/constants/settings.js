export const settingsVer = 1.0
export const detailsMap = {
  NameMessagePreview: 'Name & Message',
  NameOnly: 'Name Only',
  NoDetails: 'No Details',
  NoPopup: 'No Popup'
}
export const defaultVolume = 1
export const soundsMap = {
  'pop.mp3': 'Pop',
  'pop-ding.mp3': 'Pop Ding',
  'ding.mp3': 'Ding',
  'multi-pop.mp3': 'Multi Pop',
  'level-up.mp3': 'Level Up',
  'bloop.mp3': 'Bloop',
  'bells.mp3': 'Bells',
  'choir.mp3': 'Choir',
  'water-drop.mp3': 'Water Drop',
  'digital-ring.mp3': 'Digital Ring',
  'jug-pop.mp3': 'Jug Pop',
  'you-got-mail.mp3': 'You Got Mail',
  'whistle.mp3': 'Whistle',
  customSound: 'Custom',
  none: 'None'
}
export const defaultSound = 'pop.mp3'
export const defaultDetails = 'NameMessagePreview'
export const defaultSettings = { enabled: true, version: settingsVer }
