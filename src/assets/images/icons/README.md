# Credits for SVG Icons

Icons are from scarlab duotone collection. Hosted on svgrepo.  

[https://www.svgrepo.com/collection/scarlab-duotone-line-vectors/](https://www.svgrepo.com/collection/scarlab-oval-line-icons/)

Icons:  
- cog-svgrepo-com.svg
- play-alt-svgrepo-com.svg

These icons are copyright of their owner used under the MIT license.

[https://opensource.org/license/mit](https://opensource.org/licenses/MIT)